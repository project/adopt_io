# Adopt.io Integration Module for Drupal

The Adopt.io module provides integration with the GoAdOpt service, offering a safe and intuitive cookie banner solution for Drupal websites.

### Prerequisites

Before installing the Adopt.io Integration module, ensure that you have the following:

- A Drupal website (version 8, 9, or 10)
- Administrative permissions to install modules

## Installation
### Steps to Install
1. **Get your website code:**
- Go to Adopt website (https://goadopt.io/) and get your website code

2. **Install the Module:**
- Run `composer require 'drupal/adopt_io'`

3. **Enable the Module:**
- Log in to your Drupal website as an administrator.
- Go to `Extend` in the administration menu (`/admin/modules`). Scroll down to the `Adopt Io Integration` module and check the box next to it or.
- Or using drush, run `drush en adopt_io`.

4. **Configure the Module:**
- After enabling the module, navigate to `Configuration` > `Web services` > `Adopt.io Integration settings` (or `admin/config/services/adopt_io`).
- Enter your Adopt.io website code in the provided field.
- Click the `Save configuration` button to save your changes.

## Support
For any questions, issues, or feature requests related to the Adopt.io Integration module.

## License
This module is licensed under the GNU General Public License, version 2 or later. See the [LICENSE](LICENSE) file for details.
