<?php
namespace Drupal\adopt_io\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class AdoptIoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['adopt_io.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'adopt_io_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('adopt_io.settings');

    $form['adopt_io_website_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Adopt.io Website Code'),
      '#default_value' => $config->get('adopt_io_website_code'),
      '#description' => $this->t('Enter your Adopt.io website code here.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('adopt_io.settings')
      ->set('adopt_io_website_code', $values['adopt_io_website_code'])
      ->save();

    parent::submitForm($form, $form_state);
  }
}
